/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('imgmodules', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    width: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    coverimg: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    userid: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    groupid: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    height: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    resizetype: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    datas: {
      type: DataTypes.STRING(500),
      allowNull: true
    }
  }, {
    tableName: 'imgmodules'
  });
};
