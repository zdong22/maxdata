/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('smscode', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    mobilenumber: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    timestamp: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    }
  }, {
    tableName: 'smscode'
  });
};
