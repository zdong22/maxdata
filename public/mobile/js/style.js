var homeData = function(){
	this.bodyOverfowHidden=function(){
		$('body,html').css('overflow', 'hidden');
	}
	this.bodyOverfowScroll=function(){
		$('body,html').css('overflow', 'auto');
	}
}

var homePage = new homeData();
// 判断页面滚动到底部，加载新数据
$(window).scroll(function(){
　　var scrollTop = $(this).scrollTop();
　　var scrollHeight = $(document).height();
　　var windowHeight = $(this).height();
　　if(scrollTop + windowHeight - scrollHeight >= -10){
		console.log("you are in the bottom");
　　}
});

// 类型选择

$('#select').on('click', '.status_list > li', function(event) {
	event.preventDefault();
	$(this).addClass('active');
	$(this).siblings('li').removeClass('active');
});

// 详情页淡出
$('body,document').on('click', '#details_back', function(event) {
	event.preventDefault();
	homePage.bodyOverfowScroll();
	$('#details').fadeOut('fast');
});
// 选择页淡出
$('body,document').on('click', '#select_back', function(event) {
	event.preventDefault();
	homePage.bodyOverfowScroll();
	$('#select').fadeOut('fast');
});

//选择页面淡入
$('body,document').on('click', '#search', function(event) {
	event.preventDefault();
	homePage.bodyOverfowHidden();
	$('#select').fadeIn('fast');
});

//详情页
$('#main_container').on('click', '.item', function(event) {
	event.preventDefault();
	homePage.bodyOverfowHidden();
	$('#details').fadeIn('fast');
});

