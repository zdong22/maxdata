const path = require("path")
const fs = require("fs");
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const nodeModules = {};
fs.readdirSync("node_modules")
  .filter(function(x) {
    return [".bin"].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = "commonjs " + mod;
  });
module.exports = {
  mode: 'production', // production生成的是压缩的代码
  entry: './server.js',
  output: {
     path: path.resolve(__dirname, "dist"),
     filename: "musicServer.js",
     chunkFilename: "[name].chunk.js",
     libraryTarget: "commonjs"
   },
  node: {
        fs: 'empty',
       child_process: 'empty',
       tls: 'empty',
       net: 'empty'
  },
  //  optimization: {
  //     minimize: true,
  //     minimizer: [
  //         new UglifyJSPlugin()
  //     ]
  // },
 target: "node",
 externals: nodeModules,
};