'use strict';

//数据库连接
var Sql = {
	SqlUsername : 'root',
	SqlPassword : '111111',
	SqlTable:'datamax',
	port:"3306"
}

//发短信key和secret
var client = {
    appkey: '',
    appsecret: '',
    REST_URL: 'http://gw.api.taobao.com/router/rest'
}

//首页商品分类列表图标类型
var catSortOrder = '1';
//商品分类轮播图图片不是1，但是用的同一个表

//商品评价类型,0为初评，1为追评
var commentType = '0';

module.exports.Sql = Sql;
module.exports.client = client;
