'use strict';
/*
 * 数据库连接ORM ,定义表字段，表间的关系，
 */

var router    = require('express').Router();
var ErrorCode = require('./sqlapis/err');
var User      = require('./sqlapis/api_users');
var Sms 	  = require('./sqlapis/api_sms');
var ComFunc   = require('./comfunc');
var api 	  = require('./sqlapis/api_request');

router.post('/ajax',function(req, res){
	var data = {
		api:req.body.api,
		type:req.body.type,
		cityId:req.body.cityId||"",
		token:req.headers.token||""
	}
	if (req.body.api.indexOf('getProvinces')!= -1) {
		api.reqs(data,function(err,result){
			if (err) {
				res.send(ErrorCode.false)
			}else{
				
					var resdata = {
						code:'1',
						message:"success",
						data:JSON.parse(result)
					}
				res.send(resdata)
			}
		})
	}else{
		api.request(data,function(err,result){
			if (err) {
				res.send(ErrorCode.false)
			}else{
					var resdata = {
						code:'1',
						message:"success",
						data:result[0]
					}
				res.send(resdata)
			}
		})
	}
})

module.exports = router;
