'use strict';

var mysql = require('mysql');
var Sequelize = require('sequelize');
var comset = require('../comset');
var DataTypes = Sequelize;

var sequelize = new Sequelize(comset.Sql.SqlTable, comset.Sql.SqlUsername, comset.Sql.SqlPassword, {
  host: 'localhost',
  dialect: 'mariadb',
  port:comset.Sql.port,
  logging:false,
  pool: {
    max: 100,
    min: 0,
    idle: 10000
  },
});

var Models =  sequelize.define('models', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    width: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    coverimg: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    userid: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    groupid: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    height: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    resizetype: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    datas: {
      type: DataTypes.STRING(500),
      allowNull: true
    }
  }, {
    tableName: 'models'
  });

module.exports = Models;
module.exports.sequelize = sequelize;