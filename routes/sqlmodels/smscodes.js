/* jshint indent: 2 */
'use strict';

var mysql = require('mysql');
var Sequelize = require('sequelize');
var dataTypes = Sequelize;
var comset = require('../comset');
var DataTypes = Sequelize;

var sequelize = new Sequelize(comset.Sql.SqlTable, comset.Sql.SqlUsername, comset.Sql.SqlPassword, {
  host: 'localhost',
  dialect: 'mariadb',
  port:comset.Sql.port,
  logging:false,
  pool: {
    max: 100,
    min: 0,
    idle: 10000
  },
});

var SmsCode =  sequelize.define('smscode', {
    id: {
      type: DataTypes.STRING(255),
      allowNull: true,
      primaryKey: true
    },
    mobilenumber: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    code: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'smscode'
  });

module.exports = SmsCode;
module.exports.sequelize = sequelize;
