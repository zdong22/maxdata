'use strict';

var mysql = require('mysql');
var Sequelize = require('sequelize');
var comset = require('../comset');
var DataTypes = Sequelize;

var sequelize = new Sequelize(comset.Sql.SqlTable, comset.Sql.SqlUsername, comset.Sql.SqlPassword, {
  host: 'localhost',
  dialect: 'mariadb',
  port:comset.Sql.port,
  logging:false,
  pool: {
    max: 100,
    min: 0,
    idle: 10000
  },
});

var Users =  sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nickname: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    headurl: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    sessiontoken: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    grade: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    vipstarttime: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    vipendtime: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'users'
  });

module.exports = Users;
module.exports.sequelize = sequelize;