'use strict';

var mysql = require('mysql');
var Sequelize = require('sequelize');
var comset = require('../comset');
var DataTypes = Sequelize;

var sequelize = new Sequelize(comset.Sql.SqlTable, comset.Sql.SqlUsername, comset.Sql.SqlPassword, {
  host: 'localhost',
  dialect: 'mariadb',
  port:comset.Sql.port,
  logging:false,
  pool: {
    max: 100,
    min: 0,
    idle: 10000
  },
});

var SmsCode =  sequelize.define('smscode', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    mobilenumber: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    timestamp: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    }
  }, {
    tableName: 'smscode'
  });

module.exports = SmsCode;
module.exports.sequelize = sequelize;