'use strict';

var mysql = require('mysql');
var Sequelize = require('sequelize');
var comset = require('../comset');
var DataTypes = Sequelize;

var sequelize = new Sequelize(comset.Sql.SqlTable, comset.Sql.SqlUsername, comset.Sql.SqlPassword, {
  host: 'localhost',
  dialect: 'mariadb',
  port:comset.Sql.port,
  logging:false,
  pool: {
    max: 100,
    min: 0,
    idle: 10000
  },
});

var Groups =  sequelize.define('groups', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    userid: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('current_timestamp')
    },
    parentid: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    imgurl: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'groups'
  });

module.exports = Groups;
module.exports.sequelize = sequelize;