/*
 *  Eorror 设置
 *  公共参数定义，
 *  2016-12-20 by 小贝
 */


var Errors = {
   false: {
      code    : '-1',
      message : '失败'
   },
   true:{
      code    : '1',
      message : '成功'
   },
   noLogin:{
      code    : '1007',
      message : '请登录'
   },
   noUsername:{
      code    : '1000',
      message : '用户名不能为空'
   },
   noEmail: {
      code    : '1001',
      message : '邮箱不能为空'
   },
   noPassword:{
      code    : '1002',
      message : '密码不能为空'
   },
   noMobile:{
      code:'1003',
      message:'手机号不能为空'
   },
   sameUsername:{
      code:'1004',
      message:'用户名已存在'
   },
   sameMobile:{
      code:'1005',
      message:'手机号已存在'
   },
   sameEmail:{
      code:'1006',
      message:'邮箱已存在',
   },
   noUserId :{
      code:'1008',
      message:'用户Id不能为空'
   },
   noParams: {
      code:'1010',
      message :'参数不能为空'
   },
   canNotFindUser :{
      code:'1009',
      message:'无法找到该用户'
   },
   errVcode:{
      code:'1011',
      message:'验证码错误'
   },
   noVcode : {
      code : '1012',
      message : '验证码不能为空'
   },
   noEnglishLevel : {
      code : '1012',
      message : '英语水平不能为空'
   },
   noDescript : {
      code : '1012',
      message : '个人简介不能为空'
   },
   noMaxmoney : {
      code : '1012',
      message : '最大收费不能为空'
   },
   noMinmoney : {
      code : '1012',
      message : '最低收费不能为空'
   },
   loginFaile:{
      code:'1013',
      message : '用户名或密码错误'
   },
   alreadyFollow:{
      code:'1014',
      message:'已经关注'
   },
   notFollow:{
      code:'1015',
      message : '尚未关注'
   },
   noOpenId:{
      code:'1016',
      message:'openid不能为空'
   },
   noImg:{
      code:'1017',
      message:'图片不能为空'
   },
   noTitle:{
      code:'1018',
      message:'标题不能为空'
   },
   noWordcount:{
      code:'1019',
      message:'字数不能为空'
   },
   noType:{
      code:'1020',
      message:'类型不能空'
   },
   noParagraphone:{
      code:'1021',
      message:'第一段不能为空'
   },
   noParagraphtwo:{
      code:'1022',
      message:'第二段不能为空'
   },
   noParagraphthree:{
      code:'1023',
      message:'第三段不能为空'
   },
   noArticleId:{
      code:'1024',
      message:'文章Id不能为空'
   },
   noArticle:{
      code:'1025',
      message:'无法找到该文章'
   },
   alreadyGreat:{
      code:'1026',
      message:'已点赞或喜欢'
   },
   noContent:{
      code:'1027',
      message:'内容不能为空'
   },
   noCommentId:{
      code:'1028',
      message:'评论id不能为空'
   },
   noComment:{
      code:'1029',
      message:'无法找到该评论'
   },
   orderExist:{
      code:'1030',
      message:'订单已存在'
   },
   orderExist:{
      code : '1031',
      message:'订单已存在'
   },
   noKey:{
      code:'1032',
      message:'key不能为空'
   },
   orderRsfull:{
      code:'1033',
      message:'订单响应已满'
   },
   noOrderId:{
      code:'1034',
      message:'订单ID不能为空'
   },
   noOrder:{
      code :'1035',
      message:'订单不存在或已完成'
   },
   userNoRight:{
      code:'1036',
      message:'还用户无权限'
   },
   orderRsExist:{
      code :'1037',
      message:'订单响应已存在'
   },
   orderrsFreq:{
      code :'1038',
      message:'订单请求频繁'
   },
   noOrderRsId:{
      code : '1039',
      message : '请求ID不能为空'
   },
   noResponse:{
      code : '1040',
      message:'无法找到该响应'
   },
   noResponseCanceled:{
      code : '1042',
      message:'已拒绝'
   },
   canNotSelectOrder:{
      code:'1041',
      message:'无法选单'
   },
   alreadySelectedOrder:{
      code:'1043',
      message:'已选单'
   },
   alreadyRefusedOrder:{
      code:'1044',
      message:'已拒绝选单'
   },
   sql : {
      user : 'root',
      password:'111111',
      database: 'pump'
   },
   noSubjectName:{
      code:'1045',
      message:'主题名不能为空'
   },
   noSubjectEnglishName:{
      code:'1046',
      message:'主题英文不能为空'
   },
   noParentId:{
      code:'1047',
      message:'父主题不能为空'
   },
   noSubjectType:{
      code:'1048',
      message:'主题类型不能为空'
   },
   SmsError:{
      code:'1049',
      message:'发送过于频繁'
   },
   alreadyCollect:{
      code:'1050',
      message:'已收藏'
   },
   noGoods:{
      code:'1051',
      message:'无该商品'
   },
   canNotFindAd:{
      code:'1052',
      message:'无法找到该内容'
   },
   noSignee:{
      code:'1053',
      message:'无收货人姓名'
   },
   noAdress:{
      code:'1054',
      message:'无详细收货地址'
   },
   noMobile:{
      code:'1055',
      message:'手机号不能为空'
   }, 
   noShippingId:{
      code:'1056',
      message:'无配送方式id'
   }, 
   noGoodsAmount:{
      code:'1057',
      message:'无商品总金额'
   }, 
   noShippingFee:{
      code:'1058',
      message:'无配送费'
   }, 
   noGoods:{
      code:'1059',
      message:'商品已下架或删除'
   }, 
   noPayCode:{
      code:'1060',
      message:'请选择支付方式'
   }, 
   noMoney:{
      code:'1061',
      message:'余额不足'
   }, 
   noOrder:{
      code:'1062',
      message:'订单已支付或不存在'
   }, 
   noOrderComment:{
      code:'1063',
      message:'无法评价'
   },
   canNotFindOrder:{
      code:'1064',
      message:'无法执行该操作'
   },
   alreadyBackOrder:{
      code:'1065',
      message:'订单退款已申请'
   },
   canNotCancel:{
      code:'1066',
      message:'无法取消'
   },
   canNotEnd:{
       code:'1067',
      message:'无法确认或已确认'
   },
   mobileError:{
       code:'1068',
      message:'手机号格式错误'
   },
   noAffiUsername:{
      code    : '1069',
      message : '无法找到推荐人'
   },
   secquence:{
      code    : '1070',
      message : '操作频繁请稍后重试'
   },
   shopCLosed:{
      code    : '1071',
      message : '商家已打烊'
   },
   notZero:{
      code    : '1072',
      message : '金额不能为0'
   },
   affiliateClose:{
      code    : '1073',
      message : '推荐注册已关闭'
   },
   affiliateTop:{
      code    : '1074',
      message : '该用户推荐人数已达最高'
   },
   dumpAffiliate:{
      code:'1075',
      message:'已推荐'
   },
   signClose:{
      code:'1076',
      message:'已关闭注册送积分'
   },
   alreadySign:{
      code:'1077',
      message:'已签到'
   },
   bonusNotValued:{
      code:'1078',
      message:'红包不可用'
   },
   userBonusNotEnough:{
      code:'1079',
      message:'积分余额不足'
   },
   bonusTooLarge:{
      code:'1080',
      message:'积分余额不足'
   },
   bonusBiggerLarge:{
      code:'1081',
      message:'积分大于订单金额'
   }
}

module.exports = Errors;
