'use strict';

var router = require('express').Router();
var crypto = require('crypto');

var mysql = require('mysql');
var Sequelize = require('sequelize');
var ErrorCode = require('./err');
var Comset = require('../comset');

var ModelSequelize = require('../sqlmodels/models')
var sequelize =  ModelSequelize.sequelize;
var comFunc = require('../comfunc');

/*
	新增大屏模板
 */
function addModel(data,callback){
	ModelSequelize.create(data).then(function(result){
		var data = {
			code:"1",
			message:"success",
			data:result
		}
      	return callback(data);
    },function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

function modelList(params,callback){
	ModelSequelize.findAll({
		order : 'id DESC',
		// attributes:['width','','']
	}).then(function(result){
		let data = {
			code:"1",
			message:"success",
			data:result
		}
		return callback(data);
	},function(err){
		return callback(ErrorCode.false);
	})
}

/*
	获取大屏详情信息
 */
function getModel(data,callback){
	var title = data.title;
	var where = {
		id:data.id
	}
	if(data.title){
		where.$or = [
			{
				title: {
					$like:'%'+title+'%'
				}
		    }
		]
	}
	ModelSequelize.findOne({
		where:where
	}).then(function(result){
		var data = {
			code:'1',
			message:"success",
			data:result
		}
      	return callback(data);
    },function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

/*
	删除大屏
 */
function delModel(data,callback){
	ModelSequelize.destroy({
	    where:{
	      id:data.id,
	      userid:data.userid
	    }
	  }).then(function(){
	    return callback(ErrorCode.true);
	  },function(){
	  	return callback(ErrorCode.false);
	  })
}

/*
	修改大屏
*/
function editModel(params,callback){
	var data = {
		title:params.title
	}
	ModelSequelize.update(data,{
		where:{
			id:params.id,
			userid:params.userid
		}
	}).then(function(result){
		if(result[0]==0){
			return callback(ErrorCode.false)
		}else{
			return callback(ErrorCode.true);
		}
	})
}

module.exports.addModel = addModel;
module.exports.delModel = delModel;
module.exports.editModel = editModel;
module.exports.getModel = getModel;
module.exports.modelList=modelList;