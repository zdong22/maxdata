'use strict';

var router = require('express').Router();
var crypto = require('crypto');

var mysql = require('mysql');
var Sequelize = require('sequelize');
var ErrorCode = require('./err');
var Comset = require('../comset');

var ImgModulesSequelize = require('../sqlmodels/imgmodules')
var sequelize =  ImgModulesSequelize.sequelize;
var comFunc = require('../comfunc');

/*
	新增大屏
 */
function addModule(data,callback){
	ImgModulesSequelize.create(data).then(function(result){
		var data = {
			code:"1",
			message:'success',
			data:result
		}
      	return callback(data);
    },function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

function moduleList(data,callback){
	var title = data.title;
	var where = {
		userid:data.userid,
	    groupid:data.groupid
	}
	if(data.title){
		where.$or = [
			{
				title: {
					$like:'%'+title+'%'
				}
		    }
		]
	}
	ImgModulesSequelize.findAll({
		where:where,
		order : 'id DESC',
		attributes:['id','title','width','coverimg','userid','groupid','height','resizetype','createdAt','updatedAt','modelid','headimg']
	}).then(function(result){
		var resdata = {
			code:"1",
			message:"success",
			data:result
		}
		return callback(resdata);
	})
}

/*
	获取大屏详情信息
 */
function getModule(data,callback){
	var title = data.title;
	var where = {
		id:data.id
	}
	if(data.title){
		where.$or = [
			{
				title: {
					$like:'%'+title+'%'
				}
		    }
		]
	}
	ImgModulesSequelize.findOne({
		where:where
	}).then(function(result){
		var data = {
			code:'1',
			message:"success",
			data:result
		}
      	return callback(data);
    },function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

/*
	删除大屏
 */
function delModule(data,callback){
	ImgModulesSequelize.destroy({
	    where:{
	      id:data.id,
	      userid:data.userid
	    }
	  }).then(function(){
	    return callback(ErrorCode.true);
	  },function(){
	  	return callback(ErrorCode.false);
	  })
}

/*
	修改大屏
*/
function editModule(params,callback){
	var data = params;
	ImgModulesSequelize.update(data,{
		where:{
			id:params.id,
			userid:params.userid
		}
	}).then(function(result){
		if(result[0]==0){
			return callback(ErrorCode.false)
		}else{
			return callback(ErrorCode.true);
		}
	})
}

module.exports.addModule = addModule;
module.exports.delModule = delModule;
module.exports.editModule = editModule;
module.exports.getModule = getModule;
module.exports.moduleList=moduleList;