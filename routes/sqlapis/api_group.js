'use strict';

var router = require('express').Router();
var crypto = require('crypto');

var mysql = require('mysql');
var Sequelize = require('sequelize');
var ErrorCode = require('./err');
var Comset = require('../comset');

var Groups = require('../sqlmodels/groups')
var sequelize =  Groups.sequelize;
var comFunc = require('../comfunc');

/*
	新增Group
 */
function addGroup(data,callback){
	Groups.create(data).then(function(result){
      	return callback(ErrorCode.true);
    },function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

/*
	获取Group列表
 */
function groupList(data,callback){
	Groups.findAll({
		where:{
			userid:data.userid
		}
	}).then(function(result){
		var datas = {
			code:"1",
			message:"success",
			data:result
		}
		return callback(datas)
	},function(err){
    	console.log(err);
    	return callback(ErrorCode.false);
    })
}

/*
	删除Group
 */
function delGroup(data,callback){
	Groups.destroy({
	    where:{
	      id:data.id,
	      userid:data.userid
	    }
	  }).then(function(){
	    return callback(ErrorCode.true);
	  },function(){
	  	return callback(ErrorCode.false);
	  })
}

/*
	修改Group名称
 */
function editGroup(params,callback){
	var data = {
		title:params.title
	}
	Groups.update(data,{
		where:{
			id:params.id,
			userid:params.userid
		}
	}).then(function(result){
		if(result[0]==0){
			return callback(ErrorCode.false)
		}else{
			return callback(ErrorCode.true);
		}
	})
}

module.exports.addGroup = addGroup;
module.exports.delGroup = delGroup;
module.exports.editGroup = editGroup;
module.exports.groupList=groupList;