'use strict';

var request = require('request');  
var xml2js = require('xml2js');  
var crypto = require('crypto'); 
var ComFunc = require('../comfunc');
var ComSet = require('../comset');
function paysign(appid,attach,body,mch_id,nonce_str,notify_url,out_trade_no,spbill_create_ip,total_fee,trade_type) {    //统一下单签名  
    var ret = {  
        appid: appid,  
        attach: attach,  
        body: body,  
        mch_id: mch_id,  
        nonce_str: nonce_str,  
        notify_url:notify_url,  
        out_trade_no:out_trade_no,  
        spbill_create_ip:spbill_create_ip,  
        total_fee:total_fee,  
        trade_type:trade_type  
    };  
    var string = raw(ret);  
    var key = ComSet.weixinKey;  
    string = string + '&key='+key;  //key为在微信商户平台(pay.weixin.qq.com)-->账户设置-->API安全-->密钥设置      
    return crypto.createHash('md5').update(string,'utf8').digest('hex').toUpperCase();  

};  
  
function raw(args) {  
  var keys = Object.keys(args);  
  keys = keys.sort()                                
  var newArgs = {};  
  keys.forEach(function (key) {  
    newArgs[key.toLowerCase()] = args[key];  
  });  
  var string = '';  
  for (var k in newArgs) {  
    string += '&' + k + '=' + newArgs[k];  
  }  
  string = string.substr(1); 
  return string;  
};  

//获取预支付id
function pay(params,res)    //微信支付函数  
{  
    var url = "https://api.mch.weixin.qq.com/pay/unifiedorder";  
    var appid = ComSet.weixinAppid;  
    var mch_id = ComSet.weixinMchid;
    var notify_url = ComSet.payCallBackWeixinUrl;     
    var out_trade_no = ComFunc.randomNumlittle().toString()+params.trade_no;
    var total_fee = (parseFloat(params.total_amouny*100).toFixed(0));
    var attach = params.attach;
    var body = params.content;  //客户端商品描述  
    var nonce_str = params.randomNum;
    var spbill_create_ip = params.spbill_create_ip||"101.200.148.244";
    var formData = "<xml>";
    formData += "<appid>"+appid+"</appid>"; //appid  
    formData += "<attach>"+attach+"</attach>"; //附加数据  
    formData += "<body>"+body+"</body>"; //商品或支付单简要描述  
    formData += "<mch_id>"+mch_id+"</mch_id>"; //商户号  
    formData += "<nonce_str>"+nonce_str+"</nonce_str>"; //随机字符串，不长于32位  
    formData += "<notify_url>"+notify_url+"</notify_url>"; //支付成功后微信服务器通过POST请求通知这个地址  
    formData += "<out_trade_no>"+out_trade_no+"</out_trade_no>"; //订单号  
    formData += "<spbill_create_ip>"+spbill_create_ip+"</spbill_create_ip>"; //服务端ip  
    formData += "<total_fee>"+total_fee+"</total_fee>"; //金额  
    formData += "<trade_type>APP</trade_type>"; //类型APP  
    formData += "<sign>" + paysign(appid,attach,body,mch_id,nonce_str,notify_url,out_trade_no,spbill_create_ip,total_fee,'APP') + "</sign>";  
    formData += "</xml>";  
    request(  
    {  
        url : url,  
        method : 'POST',  
        body : formData
    }, function (err, response, body)  
    {  
        if (!err && response.statusCode == 200)  
        {  
           var parser = new xml2js.Parser({ trim:true, explicitArray:false, explicitRoot:false });//解析签名结果xml转json  
            parser.parseString(body, function(err, result){
                    var timeStamp = ComFunc.getTimeStamp();
                    var sign = paySignTwo(appid,nonce_str,'Sign=WXPay',mch_id,result['prepay_id'],timeStamp);//得到prepay再次签名  
                    console.log(sign)
                    res.send({code:"1",message:"success",datas: {'appid':appid, 'mch_id': mch_id, 'prepay_id': result['prepay_id']||'', 'nonce_str': nonce_str, 'time_stamp':timeStamp, 'package_value':'Sign=WXPay', 'sign': sign}});//返回客户端数据  
            });  
  
        }  
    }  
    );  
}  
function buildXML(json){  
    var builder = new xml2js.Builder();  
    return builder.buildObject(json);  
};  

function paySignTwo(appid,notifystr,packagevalue,mchid,prepayid,timestamp) {    //参数名不可改，必须严格一模一样（在此我掉坑一次）  
    var ret = {  
        appid: appid,  
        noncestr: notifystr,  
        package: packagevalue,  
        partnerid: mchid,  
        prepayid: prepayid,  
        timestamp:timestamp  
    };
    var string = raw(ret); 
    var key = ComSet.weixinKey;
    string = string + '&key='+key;  //key为在微信商户平台(pay.weixin.qq.com)-->账户设置-->API安全-->密钥设置 
    var crypto = require('crypto');
    return crypto.createHash('md5').update(string,'utf8').digest('hex').toUpperCase();  
};
module.exports.pay=pay;