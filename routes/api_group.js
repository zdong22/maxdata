'use strict';
/*
 * 大屏分组，新增、删除、修改，
 */

var router    = require('express').Router();
var ErrorCode = require('./sqlapis/err');
var User      = require('./sqlapis/api_users');
var Groups = require('./sqlapis/api_group');
var ComFunc   = require('./comfunc');
/*
  新增分组
*/
router.post('/addgroup',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				title: data.title,
				parentid:"0",
				imgurl:""
			}
			console.log(params)
			Groups.addGroup(params,function(result){
				res.send(result)
			})
		}
	})
})


/*
  获取分组列表
*/
router.post('/grouplist',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
			}
			Groups.groupList(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  删除分组
*/
router.post('/delgroup',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id
			}
			Groups.delGroup(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  修改分组
*/
router.post('/editgroup',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id,
				title:data.title
			}
			Groups.editGroup(params,function(result){
				res.send(result)
			})
		}
	})
})

module.exports = router;