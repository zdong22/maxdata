'use strict';

var router    = require('express').Router();

router.post('/data', function(req, res) {
  var data = {
  		"data":[
           {name: '海门', value: 29},
           {name: '鄂尔多斯', value: 12},
           {name: '招远', value: 12},
           {name: '舟山', value: 12},
           {name: '齐齐哈尔', value: 14}
	  ]
	}
  res.send(data);
});
router.post('/jczzt', function(req, res) {
  var data = {
	  "x":
	    [
	      "zjm",
	      "人脸",
	      "虹膜",
	      "声音"
	    ],
	  "y":
	    [
	      [
	        324, 
	        128, 
	        213, 
	        11
	      ]
	    ]
	}
  res.send(data);
});
router.post('/cszzt', function(req, res) {
  var data = {
	  "x": [
	    "5月",
	    "6",
	    "7月",
	    "8月",
	    "9月",
	    "10月"
	  ],
	  "y": [
	    [
	      53,
	      20,
	      36,
	      45,
	      103,
	      20
	    ]
	  ]
	}
  res.send(data);
});
router.post('/txt', function(req, res) {
  var data = {
	  "x":
	    [
	      "1月",
	      "ww",
	      "3月",
	      "4月",
	      "5月",
	      "6月",
	      "7月"
	    ],
	  "y":
	    [
	      [
	        1.5, 
	        2.3, 
	        24.2, 
	        22.6, 
	        22.9, 
	        12.1, 
	        2.2
	      ],
	      [
	        2.0, 
	        2.6, 
	        1.9, 
	        2.7, 
	        3.0, 
	        3.0, 
	        3.5
	      ]
	    ]
	}
  res.send(data);
});
router.post('/jdht', function(req, res) {
  var data ={
	  "value":"48",
	  "name":"指静脉"
	}
  res.send(data);
});
router.post('/jcbqt', function(req, res) {
  var data ={
	  "label": [
	    "指静脉",
	    "人脸"
	  ],
	  "x": [
	    "5月",
	    "6月",
	    "7月",
	    "8月",
	    "9月",
	    "10月"
	  ],
	  "y": [
	    [
	      50,
	      2,
	      -36,
	      -10,
	      10,
	      20
	    ],
	    [
	      -2,
	      34,
	      -45,
	      -66,
	      76,
	      -87
	    ]
	  ]
	}
  res.send(data);
});
router.post('/mgt', function(req, res) {
  var data ={
	  "x": [
	    "中国大连",
	    "北京",
	    "太原",
	    "南京"
	  ],
	  "y": [
	    111,
	    14,
	    211,
	    11
	  ],
	  "label": "值"
	}
  res.send(data);
});
router.post('/dhwb', function(req, res) {
  var data ={
	  "text": "20"
	}
  res.send(data);
});
router.post('/bg', function(req, res) {
  var data ={
	  "x": [
	    "姓名ddd",
	    "认sdf证时间",
	    "证设备",
	    "项目名称",
	    "坐标"
	  ],
	  "y": [
	    [
	      "小王",
	      "2019-122:22",
	      "人脸",
	      "青竹湖",
	      "湖南"
	    ],
	    [
	      "李利",
	      "2019-10-23 12:22:23",
	      "指静脉",
	      "青竹湖",
	      "湖南"
	    ],
	    [
	      "张恒",
	      "2019-10-23 11:22:23",
	      "虹膜",
	      "青竹湖",
	      "湖南"
	    ],
	    [
	      "王白",
	      "2019-10-23 12:18:23",
	      "指静脉",
	      "青竹湖",
	      "湖南"
	    ],
	    [
	      "张章",
	      "2019-10-23 12:20:23",
	      "指纹",
	      "青竹湖",
	      "湖南"
	    ]
	  ]
	}
  res.send(data);
});
router.post('/sz', function(req, res) {
  var data ={
	  "text": "0010323456"
	}
  res.send(data);
});
router.post('/mcsl',function(req,res){
	var data = {
	  "x":
	    [
	      "山西",
	      "河南",
	      "安徽",
	      "湖北",
	      "湖南",
	      "河北",
	      "内蒙",
	      "深圳",
	      "甘肃",
	      "宁夏"
	    ],
	  "y":
	    [
	      [
	        67, 
	        28, 
	        21, 
	        11,
	        61, 
	        128, 
	        21, 
	        11,
	        21, 
	        111
	      ]
	    ]
	}
	res.send(data);
})
router.post('/cll',function(req,res){
	var data = {
	  "x":
	    [
	      "1月",
	      "2月",
	      "3月",
	      "4月",
	      "5月",
	      "6月",
	      "7月",
	      "8月",
	      "9月",
	      "10月",
	      "11月",
	      "12月"
	    ],
	  "y":
	    [
	      [
	        44, 
	        28, 
	        21, 
	        11,
	        33, 
	        11, 
	        21, 
	        11,
	        21, 
	        12,
	         21, 
	        11
	      ]
	    ]
	}
	res.send(data)
})
router.post('/jzbzzt', function(req, res) {
  var data ={
	  "label": [
	    "到期时间"
	  ],
	  "x": [
	    "5",
	    "6",
	    "7",
	    "8",
	    "9",
	    "10"
	  ],
	  "y": [
	    [
	      15,
	      20,
	      36,
	      10,
	      22,
	      20
	    ],
	    [
	      15,
	      20,
	      36,
	      22,
	      10,
	      20
	    ]
	  ]
  }
  res.send(data);
});
router.post('/sbyxzt',function(req,res){
	var data = {
	  "label": [
	    "指静脉",
	    "人脸"
	  ],
	  "x": [
	    "5月",
	    "6月",
	    "7月",
	    "8月",
	    "9月",
	    "10月"
	  ],
	  "y": [
	    [
	      50,
	      20,
	      36,
	      10,
	      10,
	      20
	    ],
	    [
	      20,
	      34,
	      45,
	      66,
	      76,
	      87
	    ]
	  ]
	}
	res.send(data)
})
router.post('/rlt', function(req, res) {
  var data ={
	  "x": [
	    "正常",
	    "异常"
	  ],
	  "y": [
	    [
	      91,
	      13
	    ],
	    [
	      91,
	      13
	    ],
	    [
	      91,
	      13
	    ],
	    [
	      1,
	      13
	    ],
	    [
	      24,
	      18
	    ],
	    [
	      9,
	      18
	    ],
	    [
	      7,
	      18
	    ],
	    [
	      7,
	      18
	    ],
	    [
	      8,
	      17,
	      14
	    ],
	    [
	      10,
	      15
	    ],
	    [
	      14,
	      17
	    ],
	    [
	      15,
	      17
	    ],
	    [
	      16,
	      17
	    ],
	    [
	      14,
	      17
	    ],
	    [
	      11,
	      15
	    ],
	    [
	      10,
	      17
	    ],
	    [
	      18,
	      17
	    ],
	    [
	      15,
	      20
	    ],
	    [
	      8,
	      19
	    ],
	    [
	      10,
	      17
	    ],
	    [
	      17,
	      17
	    ],
	    [
	      20,
	      20
	    ],
	    [
	      20,
	      20
	    ],
	    [
	      12,
	      12
	    ],
	    [
	      12,
	      12
	    ],
	    [
	      12,
	      12
	    ],
	    [
	      16,
	      16
	    ],
	    [
	      16,
	      15
	    ],
	    [
	      15,
	      12
	    ],
	    [
	      12,
	      12
	    ],
	    [
	      10,
	      11
	    ]
	  ],
	  "label": "值"
  }
  res.send(data);
});

router.post('/sbztjk', function(req, res) {
  var data ={
	  "data": [
	    [
	      {
	        "value": 325,
	        "name": "正常"
	      },
	      {
	        "value": 340,
	        "name": "异常"
	      }
	    ],
	    [
	      {
	        "value": 35,
	        "name": "正常"
	      },
	      {
	        "value": 120,
	        "name": "异常"
	      }
	    ]
	  ],
	  "label": [
	    "设备状态",
	    "设备类型"
	  ]
	}
  res.send(data);
});

router.post('/mcgs',function(req,res){
	var data = {
	  "x": [
	    "牧场名称",
	    "圈舍个数",
	    "牛数量",
	    "实际存栏量",
	    "总重量",
	    "平均体重"
	  ],
	  "y": [
	    [
	      "海南牧场",
	      "300",
	      "1000",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ],
	    [
	      "海南牧场",
	      "13",
	      "100",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ],[
	      "海南牧场",
	      "3",
	      "1000",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ],
	    [
	      "海南牧场",
	      "3",
	      "1000",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ],
	    [
	      "海南牧场",
	      "3",
	      "1000",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ],
	    [
	      "海南牧场",
	      "3",
	      "1000",
	      "1200",
	      "120000KG",
	      "1000KG"
	    ]
	  ]
	}
	res.send(data)
})

module.exports = router;
