'use strict';
/*
 * 大屏分组，新增、删除、修改，
 */

var router    = require('express').Router();
var ErrorCode = require('./sqlapis/err');
var User      = require('./sqlapis/api_users');
var ImgModules = require('./sqlapis/api_imgmodules');
var ComFunc   = require('./comfunc');
const fs = require("fs");

/*
  新增分组
*/
router.post('/addmodule',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				title: data.title,
				modelid:data.modelid,
				width:data.width,
				height:data.height,
				coverimg:data.coverimg,
				headimg:data.coverimg,
				groupid:data.groupid,
				resizetype:data.resizetype,
				datas:data.datas
			}
			ImgModules.addModule(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  获取分组列表
*/
router.post('/modulelist',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				groupid:data.groupid
			}
			ImgModules.moduleList(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  获取大屏详情
*/
router.post('/modulecontent',function(req,res){
	var data = req.body;
	// User.getUserId(data.token,function(err,result){
	// 	if(err){
	// 		res.send(err)
	// 	}else{
			var params = {
				// userid: result.userid,
				id:data.id
			}
			if(data.title){
				params.title = data.title;
			}
			ImgModules.getModule(params,function(result){
				// 1.打开文件, 没有就创建, 以什么样的形式来打开文件 w:写 r:读
				let fd = fs.openSync('../2.json', 'w');
				var datas = {
					"id": result.data.dataValues.id,
					"title": result.data.dataValues.title,
					"modelid": result.data.dataValues.modelid,
					"width": result.data.dataValues.width,
					"coverimg": result.data.dataValues.coverimg,
					"headimg": result.data.dataValues.headimg,
					"userid": result.data.dataValues.userid,
					"groupid": result.data.dataValues.groupid,
					"height": result.data.dataValues.height,
					"resizetype": result.data.dataValues.resizetype,
					"createdAt": result.data.dataValues.createdAt,
					"updatedAt": result.data.dataValues.updatedAt,
					"datas":JSON.parse(result.data.dataValues.datas)
				}
				let str = JSON.stringify(datas,null,"\t")
				// 2.写入文件,如果要写入文件必须要用 w 方式打开
				fs.writeFileSync(fd, str);

				// 3.关闭文件资源
				fs.closeSync(fd);
				res.send(result)
			})
		// }
	// })
})

/*
  删除大屏
*/
router.post('/delmodule',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id
			}
			ImgModules.delModule(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  修改大屏
*/
router.post('/editmodule',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id
			}
			params.width=data.width;
			params.title=data.title||"未设置标题";
			params.modelid=data.modelid;
			params.height=data.height;
			params.coverimg=data.coverimg;
			params.headimg=data.headimg;
			if (data.groupid) {
				params.groupid=data.groupid;
			}
			params.resizetype=data.resizetype;
			if (data.datas) {
				params.datas=data.datas;
			}
			ImgModules.editModule(params,function(result){
				res.send(result)
			})
		}
	})
})

module.exports = router;