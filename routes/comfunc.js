'use strict';
/**
	公共函数存放地址'
*/
var ErrorCode = require('./sqlapis/err');
var crypto    = require('crypto');
var moment = require('moment');

//检测用户是否登录 
function tokenCheck(req, res, next){
	if(!req.body.token){
		res.send(ErrorCode.noLogin);
	}
	else{
		next();
	}
}

//获取ip地址
var getClientAddress=function(req) {
  return (req.headers['x-forwarded-for'] || '').split(',')[0] 
  || req.connection.remoteAddress;
};

/**
 * @Author   黄传备
 * @DateTime 2017-08-04
 * @Function 获取时间戳
 */

var getTimeStamp = function(){
	return   Math.round(new Date()/1000)
}

/**
 * @Author   黄传备
 * @DateTime 2017-08-04
 * @Function 获取时间
 */
function getTimeString() {
    return moment().format('YYYY-MM-DD HH:mm:ss');
}

function getTimeStringClear() {
    return moment().format('YYYYMMDDHHmmss');
}

//时间戳转换成年月日
function getLocalTime(nS) {     
   return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ');     
}     

function randomNum(){
	var num =  Math.ceil(Math.random()*10000)+ Math.ceil(Math.random()*10000);
	return num;
}
function randomNumlittle(){
    var num =  Math.ceil(Math.random()*10000);
    return num;
}

//获取随机虚拟券
function randomVirNum(){
	var num = Math.ceil(Math.random()*10000000).toString()+ Math.ceil(Math.random()*10000000);
	return num;
}

//获取年月日
function getLocalYearTime(nS) {     
	var data = new Date();
	var localtime = data.getFullYear().toString()+(data.getMonth()+1)+data.getDate()+data.getHours()+data.getMinutes();
  	return   localtime;
}     

//md5加密
function md5String(str){
	return crypto.createHash('md5').update(str).digest('hex');
}


/**
 * 计算两点之间的距离
 * @type {[type]}
 */
function getFlatternDistance(lat1, lng1, lat2, lng2) {
    var radLat1 = lat1 * Math.PI / 180.0;
    var radLat2 = lat2 * Math.PI / 180.0;
    var a = radLat1 - radLat2;
    var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378.137;
    s = Math.round(s * 10000) / 10000;
    return s;
};

/**
 * [checkTel 手机号校验]
 * @Author   小黄
 * @DateTime 2017-09-18
 * @param    {string}   address
 * @return   {json}     address
 * @param    {[type]}   str     [description]
 * @return   {[type]}           [description]
 */
function checkTel(str){
    var strlength=str.length;
    if(!/^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/.test(str)){
        return false;
    }else {
        return true
    }
}

//获取随机数字
function getRandomNumber(){
    var ordersn = ComFunc.getLocalYearTime()+ComFunc.randomNum();
    return ordersn;
}

module.exports.randomNumlittle=randomNumlittle;
module.exports.tokenCheck = tokenCheck;
module.exports.md5String = md5String;
module.exports.randomNum = randomNum;
module.exports.getLocalYearTime = getLocalYearTime;
module.exports.getLocalTime = getLocalTime;
module.exports.getClientIP = getClientAddress;
module.exports.getTimeStamp = getTimeStamp;
module.exports.randomVirNum = randomVirNum;
module.exports.getFlatternDistance = getFlatternDistance;
module.exports.getTimeStringClear = getTimeStringClear;
module.exports.getTimeString = getTimeString;
module.exports.checkTel = checkTel;
module.exports.getRandomNumber=getRandomNumber;