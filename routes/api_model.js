'use strict';
/*
 * 大屏分组，新增、删除、修改，
 */

var router    = require('express').Router();
var ErrorCode = require('./sqlapis/err');
var User      = require('./sqlapis/api_users');
var Model = require('./sqlapis/api_model');
var ComFunc   = require('./comfunc');

/*
  新增分组
*/
router.post('/addmodel',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				title: data.title,
				modelid:data.modelid,
				width:data.width,
				height:data.height,
				coverimg:data.coverimg,
				groupid:data.groupid,
				resizetype:data.resizetype,
				datas:data.datas
			}
			Model.addModel(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  获取分组列表
*/
router.post('/modellist',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				groupid:data.groupid
			}
			Model.modelList(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  获取分组详情
*/
router.post('/modulecontent',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id:data.id
			}
			if(data.title){
				params.title = data.title;
			}
			Model.getModel(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  删除分组
*/
router.post('/delmodule',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id
			}
			Model.delModel(params,function(result){
				res.send(result)
			})
		}
	})
})

/*
  修改分组
*/
router.post('/editmodule',ComFunc.tokenCheck,function(req,res){
	var data = req.body;
	User.getUserId(data.token,function(err,result){
		if(err){
			res.send(err)
		}else{
			var params = {
				userid: result.userid,
				id: data.id
			}
			if (data.width) {
				params.title=data.title;
			}
			if (data.modelid) {
				params.modelid=data.modelid;
			}
			if (data.height) {
				params.height=data.height;
			}
			if (data.coverimg) {
				params.coverimg=data.coverimg;
			}
			if (data.groupid) {
				params.groupid=data.groupid;
			}
			if (data.resizetype) {
				params.resizetype=data.resizetype;
			}
			if (data.datas) {
				params.datas=data.datas;
			}
			Model.editModel(params,function(result){
				res.send(result)
			})
		}
	})
})

module.exports = router;