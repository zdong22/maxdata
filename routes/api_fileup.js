'use strict';
var router    = require('express').Router();
var Fileup = require('./sqlapis/fileup');
var busboy = require('connect-busboy');
var ComFunc   = require('./comfunc');

router.use(busboy());
// 上传图片到七牛云存储
router.post('/uploadfile',function(req,res){
	Fileup.uploadFile(req,function(result){
		console.log(result);
		res.send(result)
	})
})

module.exports = router;
